var app = new Vue({
    el: '#app',
    data: {
        name:" X p a n d ",
        background_image:'img/66624.jpg',
        avatar:'img/xpand.jpg',
        str1:" 我 是 碼 农 ",
        str2:" 也 是 民 間 手 艺 人 ",
        str3:" 我 是 程 序 猿 ",
        str4:" 也 是 艺 術 家 ",
        about_p1:'我是一个爱编程，爱游戏，爱运动，唯独不爱说话的程序猿！',
        about_p2:'嘿嘿嘿，下面这张图是我随便找的，我们程序猿的祖师爷！',
        // 技能 和 百分比
        skill1:'HTML / CSS3',
        skill1_num:'60%',
        skill2:'Javascript',
        skill2_num:'90%',
        skill3:'VUE',
        skill3_num:'60%',
        skill4:'MySQL / NoSQL',
        skill4_num:'70%',
        skill5:'JAVA',
        skill5_num:'95%',
        skill6:'LINUX',
        skill6_num:'80%',
        work_years:1, // 工作年限
        project_num:-1, // 项目总数
        code_line_num:1433223,// 总代码量,
        school1:'共青科技职业学院', // 学校1
        major1:'软件技术', // 专业1
        major_dtl1:'主要研究物质世界微观粒子运动规律的物理学分支，主要研究原子、分子、凝聚态物质，以及原子核和基本粒子的结构、性质的基础理论它与相对论一起构成现代物理学的理论基础。', // 简介1
        school2:'家里蹲大学', // 学校2
        major2:'玄学', // 专业2
        major_dtl2:'每一个人在本性上都想求知。', // 简介2
        years1:'2017 - 2020',
        years2:'2020 - 至今',
        job1:'研发',// 职位
        jobp1:'疑难杂症妙手回春', // 工作简介
        cpn1:'Google Labs', // 公司
        cpnyear1:'2015 - 2017',
        job2:'开发',
        jobp2:'从0到1',
        cpn2:'Facebook',
        cpnyear2:'2017 - 2018'
    }
})